package gol.data;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public interface Draggable {
    String RECTANGLE = "RECTANGLE";
    String ELLIPSE = "ELLIPSE";
    golState getStartingState();
    void start(int x, int y);
    void drag(int x, int y);
    void size(int x, int y);
    double getX();
    double getY();
    double getWidth();
    double getHeight();
    void setLocationAndSize(double initX, double initY, double initWidth, double initHeight);
    String getShapeType();
}
